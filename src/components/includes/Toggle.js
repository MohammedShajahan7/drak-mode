import React from 'react'
import styled from 'styled-components'
import { ReactComponent as Moon } from '../../assets/moon.svg'
import { ReactComponent as Sun } from '../../assets/sun.svg'

function Toggle({ theme, toggleTheme }) {
    console.log(theme);
    return (
        <div onClick={toggleTheme}>
            { 
            theme === 'light' ? 
             <ThemeIcon><Moon /></ThemeIcon> 
            :
             <ThemeIcon><Sun /></ThemeIcon> 
            }
        </div>
    )
}

const ThemeIcon= styled.svg`
  position: absolute;
  top: 1rem;
  right: 4rem;
  transition: all .5s linear;
`;


export default Toggle

import React from "react";

export const Content = () => {
  return (
    <div>
      <h1> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident
        reprehenderit dolorem illo natus consequuntur perferendis. Rem fuga qui
        dolorum adipisci laboriosam dolore id odit praesentium, consequatur
      </p>
      <button className="btn-primary">Read More</button>
    </div>
  );
};
